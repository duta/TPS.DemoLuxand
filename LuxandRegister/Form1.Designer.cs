﻿namespace LuxandRegister
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.CBLog = new System.Windows.Forms.CheckBox();
            this.CBTambah = new System.Windows.Forms.CheckBox();
            this.CBAttendance = new System.Windows.Forms.CheckBox();
            this.CBDaftar = new System.Windows.Forms.CheckBox();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splitContainer1.IsSplitterFixed = true;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.BackColor = System.Drawing.SystemColors.ControlLight;
            this.splitContainer1.Panel1.Controls.Add(this.CBLog);
            this.splitContainer1.Panel1.Controls.Add(this.CBTambah);
            this.splitContainer1.Panel1.Controls.Add(this.CBAttendance);
            this.splitContainer1.Panel1.Controls.Add(this.CBDaftar);
            this.splitContainer1.Size = new System.Drawing.Size(654, 682);
            this.splitContainer1.SplitterDistance = 30;
            this.splitContainer1.TabIndex = 0;
            // 
            // CBLog
            // 
            this.CBLog.Appearance = System.Windows.Forms.Appearance.Button;
            this.CBLog.AutoSize = true;
            this.CBLog.Location = new System.Drawing.Point(295, 3);
            this.CBLog.Name = "CBLog";
            this.CBLog.Size = new System.Drawing.Size(35, 23);
            this.CBLog.TabIndex = 3;
            this.CBLog.Text = "Log";
            this.CBLog.UseVisualStyleBackColor = true;
            this.CBLog.CheckedChanged += new System.EventHandler(this.CBLog_CheckedChanged);
            // 
            // CBTambah
            // 
            this.CBTambah.Appearance = System.Windows.Forms.Appearance.Button;
            this.CBTambah.AutoSize = true;
            this.CBTambah.Location = new System.Drawing.Point(105, 3);
            this.CBTambah.Name = "CBTambah";
            this.CBTambah.Size = new System.Drawing.Size(106, 23);
            this.CBTambah.TabIndex = 2;
            this.CBTambah.Text = "Tambah Karyawan";
            this.CBTambah.UseVisualStyleBackColor = true;
            this.CBTambah.CheckedChanged += new System.EventHandler(this.CBTambah_CheckedChanged);
            // 
            // CBAttendance
            // 
            this.CBAttendance.Appearance = System.Windows.Forms.Appearance.Button;
            this.CBAttendance.AutoSize = true;
            this.CBAttendance.Location = new System.Drawing.Point(217, 3);
            this.CBAttendance.Name = "CBAttendance";
            this.CBAttendance.Size = new System.Drawing.Size(72, 23);
            this.CBAttendance.TabIndex = 1;
            this.CBAttendance.Text = "Attendance";
            this.CBAttendance.UseVisualStyleBackColor = true;
            this.CBAttendance.CheckedChanged += new System.EventHandler(this.CBAttendance_CheckedChanged);
            // 
            // CBDaftar
            // 
            this.CBDaftar.Appearance = System.Windows.Forms.Appearance.Button;
            this.CBDaftar.AutoSize = true;
            this.CBDaftar.Checked = true;
            this.CBDaftar.CheckState = System.Windows.Forms.CheckState.Checked;
            this.CBDaftar.Location = new System.Drawing.Point(3, 3);
            this.CBDaftar.Name = "CBDaftar";
            this.CBDaftar.Size = new System.Drawing.Size(96, 23);
            this.CBDaftar.TabIndex = 0;
            this.CBDaftar.Text = "Daftar Karyawan";
            this.CBDaftar.UseVisualStyleBackColor = true;
            this.CBDaftar.CheckedChanged += new System.EventHandler(this.CBDaftar_CheckedChanged);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(654, 682);
            this.Controls.Add(this.splitContainer1);
            this.Name = "Form1";
            this.Text = "Face Recognition Attendance";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Form1_FormClosed);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.PerformLayout();
            this.splitContainer1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.CheckBox CBAttendance;
        private System.Windows.Forms.CheckBox CBDaftar;
        private System.Windows.Forms.CheckBox CBTambah;
        private System.Windows.Forms.CheckBox CBLog;
    }
}

