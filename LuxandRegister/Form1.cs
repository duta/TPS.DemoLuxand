﻿using System;
using System.Windows.Forms;

namespace LuxandRegister
{
    public partial class Form1 : Form
    {
        private DaftarKaryawan daftar;
        private TambahKaryawan tambah;
        private UpdateKaryawan update;
        private Attendance attend;
        private LogAttendance log;

        public Form1()
        {
            InitializeComponent();
            ShowDaftar();
        }

        private void CBDaftar_CheckedChanged(object sender, EventArgs e)
        {
            if (CBDaftar.Checked)
            {
                ShowDaftar();
            }
        }

        public void ShowDaftar()
        {
            if (daftar == null)
            {
                daftar = new DaftarKaryawan();
                daftar.Dock = DockStyle.Fill;
            }
            if (update != null)
            {
                update.needClose = true;
                update.Dispose();
            }
            if (tambah != null)
            {
                tambah.needClose = true;
                //tambah.Dispose();
            }
            if (attend != null)
            {
                //attend.needClose = true;
                //attend.Dispose();
            }
            JPEGReceiver.StopContinuousFetch();
            daftar.Reload();
            CBDaftar.Checked = true;
            CBAttendance.Checked = false;
            CBTambah.Checked = false;
            CBLog.Checked = false;
            splitContainer1.Panel2.Controls.Clear();
            splitContainer1.Panel2.Controls.Add(daftar);
        }

        public void ShowUpdate(Karyawan k)
        {
            JPEGReceiver.StopContinuousFetch();
            update = new UpdateKaryawan(k);
            update.Dock = DockStyle.Fill;
            splitContainer1.Panel2.Controls.Clear();
            splitContainer1.Panel2.Controls.Add(update);
        }

        private void CBTambah_CheckedChanged(object sender, EventArgs e)
        {
            if (CBTambah.Checked)
            {
                JPEGReceiver.StopContinuousFetch();
                if (attend != null)
                {
                    //attend.needClose = true;
                    //attend.Dispose();
                }
                if (update != null)
                {
                    update.needClose = true;
                    update.Dispose();
                }
                CBAttendance.Checked = false;
                CBDaftar.Checked = false;
                CBLog.Checked = false;

                if (tambah != null) {
                    tambah.Dispose();
                    GC.Collect();
                } 

                tambah = new TambahKaryawan();
                tambah.Dock = DockStyle.Fill;
                splitContainer1.Panel2.Controls.Clear();
                splitContainer1.Panel2.Controls.Add(tambah);
            }
        }

        private void CBAttendance_CheckedChanged(object sender, EventArgs e)
        {
            if (CBAttendance.Checked)
            {
                JPEGReceiver.StopContinuousFetch();
                if (tambah != null)
                {
                    //tambah.needClose = true;
                    //tambah.Dispose();
                }
                if (update != null)
                {
                    update.needClose = true;
                    update.Dispose();
                }
                CBTambah.Checked = false;
                CBDaftar.Checked = false;
                CBLog.Checked = false;
                if (attend == null) attend = new Attendance();                
                attend.Dock = DockStyle.Fill;
                
                splitContainer1.Panel2.Controls.Clear();
                splitContainer1.Panel2.Controls.Add(attend);

                attend.TBNIP.Focus();
            }
        }

        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            JPEGReceiver.StopContinuousFetch();
            if (tambah != null)
            {
                tambah.needClose = true;
                tambah.Dispose();
            }
            if (update != null)
            {
                update.needClose = true;
                update.Dispose();
            }
            if (attend != null)
            {
                attend.needClose = true;
                attend.Dispose();
            }
        }

        private void CBLog_CheckedChanged(object sender, EventArgs e)
        {
            JPEGReceiver.StopContinuousFetch();
            if (CBLog.Checked)
            {
                if (tambah != null)
                {
                    tambah.needClose = true;
                    //tambah.Dispose();
                }
                if (update != null)
                {
                    update.needClose = true;
                    update.Dispose();
                }
                if (attend != null)
                {
                    //attend.needClose = true;
                    //attend.Dispose();
                }
                if (log == null)
                {
                    log = new LogAttendance();
                    log.Dock = DockStyle.Fill;
                }
                log.Reload();
                CBTambah.Checked = false;
                CBDaftar.Checked = false;
                CBAttendance.Checked = false;
                splitContainer1.Panel2.Controls.Clear();
                splitContainer1.Panel2.Controls.Add(log);
            }
        }
    }
}