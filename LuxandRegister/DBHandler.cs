﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.SQLite;
using System.IO;
using System.Text;

namespace LuxandRegister
{
    static class DBHandler
    {
        static byte[] GetBytes(SQLiteDataReader reader)
        {
            const int CHUNK_SIZE = 2 * 1024;
            byte[] buffer = new byte[CHUNK_SIZE];
            long bytesRead;
            long fieldOffset = 0;
            using (MemoryStream stream = new MemoryStream())
            {
                while ((bytesRead = reader.GetBytes(2, fieldOffset, buffer, 0, buffer.Length)) > 0)
                {
                    stream.Write(buffer, 0, (int)bytesRead);
                    fieldOffset += bytesRead;
                }
                return stream.ToArray();
            }
        }
        public static List<Karyawan> GetKaryawan()
        {
            List<Karyawan> result = new List<Karyawan>();
            try
            {
                using (var connection = new SQLiteConnection("Data Source=datafile.db"))
                using (var command = new SQLiteCommand("SELECT nip, nama, tracker, rfid FROM karyawan", connection))
                {
                    connection.Open();
                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            var k = new Karyawan();
                            k.nip = reader.GetString(0);
                            k.nama = reader.GetString(1);
                            k.buffer = GetBytes(reader);
                            k.rfid = reader.GetString(3);
                            result.Add(k);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return result;
        }
        public static Karyawan GetOneKaryawan(string nip)
        {
            try
            {
                using (var connection = new SQLiteConnection("Data Source=datafile.db"))
                using (var command = new SQLiteCommand("SELECT nip, nama, tracker, rfid FROM karyawan WHERE nip=@nip", connection))
                {
                    connection.Open();
                    command.Parameters.AddWithValue("nip", nip);
                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            var k = new Karyawan();
                            k.nip = reader.GetString(0);
                            k.nama = reader.GetString(1);
                            k.buffer = GetBytes(reader);
                            k.rfid = reader.GetString(3);
                            return k;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return null;
        }

        public static Karyawan GetOneKaryawanByRfid(string rfid)
        {
            try
            {
                using (var connection = new SQLiteConnection("Data Source=datafile.db"))
                using (var command = new SQLiteCommand("SELECT nip, nama, tracker, rfid FROM karyawan WHERE rfid=@rfid", connection))
                {
                    connection.Open();
                    command.Parameters.AddWithValue("rfid", rfid);
                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            var k = new Karyawan();
                            k.nip = reader.GetString(0);
                            k.nama = reader.GetString(1);
                            k.buffer = GetBytes(reader);
                            k.rfid = reader.GetString(3);
                            return k;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return null;
        }

        public static void AddKaryawan(string nip, string nama, byte[] tracker, string rfid)
        {
            using (var connection = new SQLiteConnection("Data Source=datafile.db"))
            using (var command = new SQLiteCommand("INSERT INTO karyawan (nip, nama, tracker, rfid) VALUES (@nip, @nama, @tracker, @rfid)", connection))
            {
                connection.Open();
                command.Parameters.AddWithValue("nip", nip);
                command.Parameters.AddWithValue("nama", nama);
                command.Parameters.Add("@tracker", System.Data.DbType.Binary, 20).Value = tracker;
                command.Parameters.AddWithValue("rfid", rfid);
                command.ExecuteNonQuery();
            }
        }
        public static void RemoveKaryawan(string nip)
        {
            using (var connection = new SQLiteConnection("Data Source=datafile.db"))
            using (var command = new SQLiteCommand("DELETE FROM karyawan WHERE nip=@nip", connection))
            {
                connection.Open();
                command.Parameters.AddWithValue("nip", nip);
                command.ExecuteNonQuery();
            }
        }

        internal static void UpdateKaryawan(Karyawan k)
        {
            using (var connection = new SQLiteConnection("Data Source=datafile.db"))
            using (var command = new SQLiteCommand("UPDATE karyawan SET tracker=@tracker WHERE nip=@nip", connection))
            {
                connection.Open();
                command.Parameters.Add("@tracker", System.Data.DbType.Binary, 20).Value = k.buffer;
                command.Parameters.AddWithValue("nip", k.nip);
                command.ExecuteNonQuery();
            }
        }
        public static void AddLog(string nip, DateTime date)
        {
            using (var connection = new SQLiteConnection("Data Source=datafile.db"))
            using (var command = new SQLiteCommand("INSERT INTO log (nip, date) VALUES (@nip, @date)", connection))
            {
                connection.Open();
                command.Parameters.AddWithValue("nip", nip);
                command.Parameters.AddWithValue("date", date.ToString("dd-MM-yyyy HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture));
                command.ExecuteNonQuery();
            }
        }
        public static Dictionary<Karyawan, DateTime> GetLog()
        {
            Dictionary<Karyawan, DateTime> result = new Dictionary<Karyawan, DateTime>();
            try
            {
                using (var connection = new SQLiteConnection("Data Source=datafile.db"))
                using (var command = new SQLiteCommand("SELECT nip, date FROM log", connection))
                {
                    connection.Open();
                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            result.Add(GetOneKaryawan(reader.GetString(0)),
                                DateTime.ParseExact(reader.GetString(1), "dd-MM-yyyy HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture));
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return result;
        }
    }

    public class Karyawan
    {
        public string nip;
        public string nama;
        public byte[] buffer;
        public string rfid;
    }
}
