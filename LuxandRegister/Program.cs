﻿using System;
using System.Windows.Forms;
using System.Configuration;


namespace LuxandRegister
{
    static class Program
    {
        public static string UrlJpgCamera = "";
        public static int PortBroadcastMjpeg;

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            JPEGReceiver.source = ConfigurationManager.AppSettings["url-source-jpg-camera"].ToString();
            //PortBroadcastMjpeg = 3579;//int.Parse(ConfigurationManager.AppSettings["port-broadcast-source-to-mjpeg"].ToString());


            //new StreamMjpeg(Program.PortBroadcastMjpeg, Program.UrlJpgCamera);

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form1());
        }

        
    }

}
