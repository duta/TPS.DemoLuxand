﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using Luxand;

namespace LuxandRegister
{
    public partial class Attendance : UserControl
    {
        int programRemember = 0;
        public bool needClose = false;
        string nip = null;
        string nama = null;
        int tracker = 0;

        // WinAPI procedure to release HBITMAP handles returned by FSDKCam.GrabFrame
        [DllImport("gdi32.dll")]
        static extern bool DeleteObject(IntPtr hObject);
        public Attendance()
        {
            InitializeComponent();
            this.Load += Attendance_Load1;            
            TBNIP.KeyUp += TBNIP_KeyUp;
            
        }

        private void Attendance_Load1(object sender, EventArgs e)
        {
            TBNIP.Focus();
        }

        private void TBNIP_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Return)
            {
                if (string.IsNullOrEmpty(TBNIP.Text)) return;
                BtnTrain_Click(null, null);
            }
        }

        private void BtnTrain_Click(object sender, EventArgs e)
        {
            programRemember = 150; //buat value ini lebih besar untuk membaca wajah lebih lama
            Karyawan k = DBHandler.GetOneKaryawanByRfid(TBNIP.Text);
            if (k == null)
            {
                MessageBox.Show("Karyawan NIP " + TBNIP.Text + " tidak terdaftar.");
                TBNIP.Text = "";
                TBNIP.Focus();
                return;
            }
            nip = k.nip;
            nama = k.nama;

            TBNIP.Text = k.nip;

            try
            {
                FSDK.LoadTrackerMemoryFromBuffer(ref tracker, k.buffer);

                int err = 0; // set realtime face detection parameters
                FSDK.SetTrackerMultipleParameters(tracker, "HandleArbitraryRotations=true; DetermineFaceRotationAngle=false; InternalResizeWidth=300; FaceDetectionThreshold=2;", ref err);

                while (programRemember > 0)
                {
                    FSDK.CImage image = new FSDK.CImage(JPEGReceiver.FetchOne());

                    long[] IDs;
                    long faceCount = 0;
                    FSDK.FeedFrame(tracker, 0, image.ImageHandle, ref faceCount, out IDs, sizeof(long) * 256); // maximum of 256 faces detected
                    Array.Resize(ref IDs, (int)faceCount);

                    // make UI controls accessible (to find if the user clicked on a face)
                    Application.DoEvents();

                    Image frameImage = image.ToCLRImage();
                    Graphics gr = Graphics.FromImage(frameImage);

                    for (int i = 0; i < IDs.Length; ++i)
                    {
                        FSDK.TFacePosition facePosition = new FSDK.TFacePosition();
                        FSDK.GetTrackerFacePosition(tracker, 0, IDs[i], ref facePosition);

                        int left = facePosition.xc - (int)(facePosition.w * 0.6);
                        int top = facePosition.yc - (int)(facePosition.w * 0.5);
                        int w = (int)(facePosition.w * 1.2);

                        String found;
                        int res = FSDK.GetAllNames(tracker, IDs[i], out found, 65536); // maximum of 65536 characters

                        if (programRemember > 0 && FSDK.FSDKE_OK == res && found.Length > 0 && found.Equals(nip))
                        {
                            DBHandler.AddLog(nip, DateTime.Now);
                            programRemember = 0;

                            AutoClosingMessageBox.Show("Terimakasih " + nama + ", Anda absen tgl/jam " + DateTime.Now,
                                "Konfirmasi", 3000);
                        }

                        Pen pen = Pens.LightGreen;
                        gr.DrawRectangle(pen, left, top, w, w);
                    }
                    if (programRemember > 0)
                    {
                        Console.WriteLine("programRemember " + programRemember);
                        if (programRemember == 1) AutoClosingMessageBox.Show("Wajah tidak sesuai dengan NIP",
                            "Konfirmasi", 3000);
                        programRemember -= 1;
                    }

                    // display current frame
                    pictureBox1.Image = frameImage;
                    GC.Collect(); // collect the garbage after the deletion
                }
                FSDK.FreeTracker(tracker);
                pictureBox1.Image = null;

                TBNIP.Text = "";
                TBNIP.Focus();
            }
            catch (Exception ex)
            {
                ex.Message.ToString();
            }
        }

        private void Attendance_Load(object sender, EventArgs e)
        {
            //if (FSDK.FSDKE_OK != FSDK.ActivateLibrary("r9GpYU8KGITrQZvs9whh3kk/rUPOZzh97KU/pepTb5ihqdl9PcZvQP0iZKnpWVOVkVbYhcCKL+T6S7P6i4ZaLzUfaS+nxNu4ofCi8ucxwE2evJVp9ILLG6RObRPidXSYNGj11RPzuJr/7E9N4cMbH2ZlYIvVu3s7/1Oo+CecLp0="))
            if (FSDK.FSDKE_OK != FSDK.ActivateLibrary("C3l+tyHRzKlljCZW2y4GUiF+Ifpja2shD+dZT4mJLIxDxHcunyDjZNeec1S0swGwjb8Av68dgnJGMgoZBWBmorydT9HBYPHN2mCNopv/IBYe3BrKCcnDvg/kEgrfmJqY6V/VqqFRxkf41uokg54yNV6GJJcFhb9G0P7E+gBpdt0="))
            {
                MessageBox.Show("Please run the License Key Wizard (Start - Luxand - FaceSDK - License Key Wizard)", "Error activating FaceSDK", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            FSDK.InitializeLibrary();
        }
    }
}
