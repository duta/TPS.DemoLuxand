﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace LuxandRegister
{
    public partial class DaftarKaryawan : UserControl
    {
        public DaftarKaryawan()
        {
            InitializeComponent();
        }

        private void DaftarKaryawan_Load(object sender, EventArgs e)
        {
            Reload();
        }

        public void Reload()
        {
            List<Karyawan> lk = DBHandler.GetKaryawan();
            dataGridView1.Rows.Clear();
            foreach (var item in lk)
            {
                dataGridView1.Rows.Add(new object[] { item.nip, item.nama, item.rfid, "Update", "Delete" });
            }
        }

        private void DaftarKaryawan_Enter(object sender, EventArgs e)
        {
            Reload();
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            var senderGrid = (DataGridView)sender;

            if (senderGrid.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString().Equals("Delete"))
            {
                DBHandler.RemoveKaryawan(senderGrid.Rows[e.RowIndex].Cells[0].Value.ToString());
                Reload();
            }

            try
            {
                if (senderGrid.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString().Equals("Update"))
                {
                    ((Form1)this.ParentForm).ShowUpdate(DBHandler.GetOneKaryawan(senderGrid.Rows[e.RowIndex].Cells[0].Value.ToString()));
                }
            }
            catch (Exception)
            {
            }
            
        }
    }
}
