﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Drawing;
using System.Net.Sockets;
using AForge.Video;

namespace MagicJpegService
{
    public class MjpegStreamer : IDisposable
    {
        private List<NetworkStream> Streams;
        private string BoundText;
        private byte[] headData;
        public JPEGStream jpegstream;
        public System.Windows.Forms.PictureBox Display { get; set; }
        
        public MjpegStreamer(string jpegUrl)
        {
            Streams = new List<NetworkStream>();
            BoundText = "--boundary";
            headData = BytesOf("HTTP/1.1 200 OK\r\n" +
                    "Content-Type: multipart/x-mixed-replace; boundary=" +
                    BoundText +
                    "\r\n");
            jpegstream = new JPEGStream(jpegUrl);
            jpegstream.NewFrame += new NewFrameEventHandler(stream_NewFrame);
            jpegstream.Start();
        }

        public void stream_NewFrame(object sender, NewFrameEventArgs eventArgs)
        {
            Write(eventArgs.Frame);
            if (Display != null)
            {
                try
                {
                    Display.Image = (Image)eventArgs.Frame.Clone();
                    Display.Hide();
                    Display.Show();
                }
                catch { }
            }
        }

        public void AddStream(NetworkStream stream)
        {
            WriteHeader(stream);
            Streams.Add(stream);
        }

        public void WriteHeader(Stream s)
        {
            s.Write(headData, 0, headData.Length);
            s.Flush();
        }

        public void Write(Image image)
        {
            MemoryStream ms = BytesOf(image);
            Write(ms);
        }

        public void Write(MemoryStream imageStream)
        {
            StringBuilder sb = new StringBuilder();

            sb.AppendLine();
            sb.AppendLine(BoundText);
            sb.AppendLine("Content-Type: image/jpeg");
            sb.AppendLine("Content-Length: " + imageStream.Length.ToString());
            sb.AppendLine();

            Write(sb.ToString());
            foreach (Stream s in Streams)
            {
                imageStream.WriteTo(s);
            }
            Write("\r\n");

            foreach (Stream s in Streams)
            {
                s.Flush();
            }
        }

        private void Write(byte[] data)
        {
            List<NetworkStream> cleanup = null;
            foreach (NetworkStream s in Streams)
            {
                try
                {
                    s.Write(data, 0, data.Length);
                }
                catch (Exception e)
                {
                    if (cleanup == null) cleanup = new List<NetworkStream>();
                    cleanup.Add(s);
                }
            }
            if (cleanup != null)
            {
                foreach (NetworkStream s in cleanup)
                {
                    Streams.Remove(s);
                }
            }
        }

        private void Write(string text)
        {
            byte[] data = BytesOf(text);
            List<NetworkStream> cleanup = null;
            foreach (NetworkStream s in Streams)
            {
                try
                {
                    s.Write(data, 0, data.Length);
                }
                catch (Exception e)
                {
                    if (cleanup == null) cleanup = new List<NetworkStream>();
                    cleanup.Add(s);
                }
            }
            if (cleanup != null)
            {
                foreach (NetworkStream s in cleanup)
                {
                    Streams.Remove(s);
                }
            }
        }

        private static byte[] BytesOf(string text)
        {
            return Encoding.ASCII.GetBytes(text);
        }

        private static MemoryStream BytesOf(Image image)
        {
            MemoryStream ms = new MemoryStream();
            image.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg);
            return ms;
        }

        #region IDisposable Members

        public void Dispose()
        {
            try
            {
                jpegstream.Stop();
                if (Streams.Count > 0)
                    foreach (Stream s in Streams)
                    {
                        s.Dispose();
                    }
            }
            finally
            {
            }
        }

        #endregion
    }
}
