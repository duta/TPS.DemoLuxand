﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using Luxand;

namespace LuxandRegister
{
    public partial class UpdateKaryawan : UserControl
    {
        bool programRemember = false;
        String cameraName;
        public bool needClose = false;
        int mouseX = 0;
        int mouseY = 0;
        private int tracker;

        public Karyawan k { get; set; }

        // WinAPI procedure to release HBITMAP handles returned by FSDKCam.GrabFrame
        [DllImport("gdi32.dll")]
        static extern bool DeleteObject(IntPtr hObject);
        public UpdateKaryawan(Karyawan k)
        {
            InitializeComponent();
            this.k = k;
        }

        private void TambahKaryawan_Load(object sender, EventArgs e)
        {
            try
            {
                if (FSDK.FSDKE_OK != FSDK.ActivateLibrary("C3l+tyHRzKlljCZW2y4GUiF+Ifpja2shD+dZT4mJLIxDxHcunyDjZNeec1S0swGwjb8Av68dgnJGMgoZBWBmorydT9HBYPHN2mCNopv/IBYe3BrKCcnDvg/kEgrfmJqY6V/VqqFRxkf41uokg54yNV6GJJcFhb9G0P7E+gBpdt0="))
                {
                    MessageBox.Show("Please run the License Key Wizard (Start - Luxand - FaceSDK - License Key Wizard)", "Error activating FaceSDK", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    Application.Exit();
                }

                FSDK.InitializeLibrary();

                tracker = 0;    // creating a Tracker
                if (FSDK.FSDKE_OK != FSDK.LoadTrackerMemoryFromBuffer(ref tracker, k.buffer)) // try to load saved tracker state
                {
                    MessageBox.Show("Failed to load tracker data");
                    ((Form1)this.ParentForm).ShowDaftar();
                    return;
                }

                int err = 0; // set realtime face detection parameters
                FSDK.SetTrackerMultipleParameters(tracker, "HandleArbitraryRotations=true; DetermineFaceRotationAngle=false; InternalResizeWidth=300; FaceDetectionThreshold=2;", ref err);

                TBNama.Text = k.nama;
                TBNIP.Text = k.nip;
                TBRFID.Text = k.rfid;
                JPEGReceiver.FetchedImage += JPEGReceiver_FetchedImage;
                JPEGReceiver.StartContinuousFetch();
            } catch (Exception ex)
            {
                ex.Message.ToString();
            }
        }

        private void JPEGReceiver_FetchedImage(object sender, EventArgs e)
        {
            Application.DoEvents();
            FSDK.CImage image = new FSDK.CImage((Bitmap)sender);

            long[] IDs;
            long faceCount = 0;
            FSDK.FeedFrame(tracker, 0, image.ImageHandle, ref faceCount, out IDs, sizeof(long) * 256); // maximum of 256 faces detected
            Array.Resize(ref IDs, (int)faceCount);

            Image frameImage = image.ToCLRImage();
            Graphics gr = Graphics.FromImage(frameImage);

            for (int i = 0; i < IDs.Length; ++i)
            {
                FSDK.TFacePosition facePosition = new FSDK.TFacePosition();
                FSDK.GetTrackerFacePosition(tracker, 0, IDs[i], ref facePosition);

                int left = facePosition.xc - (int)(facePosition.w * 0.6);
                int top = facePosition.yc - (int)(facePosition.w * 0.5);
                int w = (int)(facePosition.w * 1.2);

                Pen pen = Pens.LightGreen;
                if (mouseX >= left && mouseX <= left + w && mouseY >= top && mouseY <= top + w)
                {
                    pen = Pens.Blue;
                    if (programRemember)
                    {
                        if (FSDK.FSDKE_OK == FSDK.LockID(tracker, IDs[i]))
                        {
                            // draw face onto one of the free pictureboxes.
                            Bitmap target = new Bitmap(w, w);

                            using (Graphics g = Graphics.FromImage(target))
                            {
                                g.DrawImage(frameImage, new Rectangle(0, 0, target.Width, target.Height),
                                                 new Rectangle(left, top, target.Width, target.Height),
                                                 GraphicsUnit.Pixel);
                            }
                            if (pictureBox2.Image == null) pictureBox2.Image = target;
                            else if (pictureBox3.Image == null) pictureBox3.Image = target;
                            else if (pictureBox4.Image == null) pictureBox4.Image = target;
                            else if (pictureBox5.Image == null) pictureBox5.Image = target;
                            else if (pictureBox6.Image == null) pictureBox6.Image = target;
                            // get the user name
                            FSDK.SetName(tracker, IDs[i], TBNIP.Text);
                            FSDK.UnlockID(tracker, IDs[i]);
                            progressBar1.Value += 20;
                            Application.DoEvents();
                            if (progressBar1.Value >= 100)
                            {
                                long BufSize = 0;
                                FSDK.GetTrackerMemoryBufferSize(tracker, ref BufSize);
                                byte[] buffer = new byte[BufSize];
                                FSDK.SaveTrackerMemoryToBuffer(tracker, out buffer, BufSize);
                                k.buffer = buffer;
                                k.rfid = TBRFID.Text;
                                DBHandler.UpdateKaryawan(k);

                                FSDK.FreeTracker(tracker);

                                JPEGReceiver.StopContinuousFetch();

                                ((Form1)this.ParentForm).ShowDaftar();
                                FSDK.FreeTracker(tracker);
                            }
                        }
                    }
                }
                gr.DrawRectangle(pen, left, top, w, w);
            }
            programRemember = false;

            // display current frame
            pictureBox1.Image = frameImage;
            GC.Collect(); // collect the garbage after the deletion
        }

        private void pictureBox1_MouseUp(object sender, MouseEventArgs e)
        {
            programRemember = true;
        }

        private void pictureBox1_MouseMove(object sender, MouseEventArgs e)
        {
            mouseX = e.X;
            mouseY = e.Y;
        }

        private void pictureBox1_MouseLeave(object sender, EventArgs e)
        {
            mouseX = -1;
            mouseY = -1;
        }
    }
}
