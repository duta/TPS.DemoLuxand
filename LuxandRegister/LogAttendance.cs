﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace LuxandRegister
{
    public partial class LogAttendance : UserControl
    {
        public LogAttendance()
        {
            InitializeComponent();
        }

        private void DaftarKaryawan_Load(object sender, EventArgs e)
        {
            Reload();
        }

        public void Reload()
        {
            var lk = DBHandler.GetLog();
            dataGridView1.Rows.Clear();
            foreach (var item in lk)
            {
                dataGridView1.Rows.Add(new object[] { item.Key.nip, item.Key.nama, item.Key.rfid, item.Value.ToString("dd-MM-yyyy HH:mm:ss") });
            }
        }

        private void DaftarKaryawan_Enter(object sender, EventArgs e)
        {
            Reload();
        }
    }
}
