﻿using MagicJpegService;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;

namespace LuxandRegister
{
    public class StreamMjpeg
    {
        private Thread serverThread;
        private Socket Server;

        private MjpegStreamer _mjpegStreamer;
        private int _port;
        private string _urlJpeg;

        public StreamMjpeg(int port, string urlJpeg)
        {
            _port = port;
            _urlJpeg = urlJpeg;
            InitServer();
        }

        private void InitServer()
        {
            _mjpegStreamer = new MjpegStreamer(_urlJpeg);

            serverThread = new Thread(new ParameterizedThreadStart(ServerThread));
            serverThread.IsBackground = true;
            serverThread.Start(_port);
        }

        private void ServerThread(object state)
        {
            try
            {
                Server = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                Server.Bind(new IPEndPoint(IPAddress.Any, (int)state));
                Server.Listen(10);
                foreach (Socket client in Server.IncomingConnections())
                    ThreadPool.QueueUserWorkItem(new WaitCallback(ClientThread), client);
                
            }
            catch ( Exception ex)
            {
                Console.WriteLine(ex.StackTrace);
            }
            KillServer();
        }

        private void ClientThread(object client)
        {
            Socket socket = (Socket)client;
            Byte[] byteBuffer = new Byte[1024];
            socket.Receive(byteBuffer);
            string camera = ASCIIEncoding.ASCII.GetString(byteBuffer);
            //Console.WriteLine(DateTime.Now.ToString("G") + "Receive " + camera);
            _mjpegStreamer.AddStream(new NetworkStream(socket, true));
        }

        private void KillServer()
        {
            try
            {
                _mjpegStreamer.Dispose();
                Server.Close();
            }
            finally
            {
                serverThread.Abort();
            }
        }
    }
}

internal static class SocketExtensions
{
    public static IEnumerable<Socket> IncomingConnections(this Socket server)
    {
        while (true)
            yield return server.Accept();
    }
}