using System;
using System.Drawing;
using System.IO;
using System.Net;
using System.Threading;

namespace LuxandRegister
{
    public static class JPEGReceiver
    {
        private static Random rand = new Random((int)DateTime.Now.Ticks);
        private static ManualResetEvent stopEvent = new ManualResetEvent(false);
        public static event EventHandler FetchedImage = delegate { };

        // buffer size used to download JPEG image
        private const int bufferSize = 1024 * 1024;
        // size of portion to read at once
        private const int readSize = 1024;
        public static string source;
        private static bool fetching;

        public static Bitmap FetchOne()
        {
            var request = (HttpWebRequest)WebRequest.Create(source + ((source.IndexOf('?') == -1) ? '?' : '&') + "fake=" + rand.Next().ToString());
            // get response stream
            var stream = request.GetResponse().GetResponseStream();
            int read, total = 0;
            byte[] buffer = new byte[bufferSize];
            while (true)
            {
                // check total read
                if (total > bufferSize - readSize)
                {
                    total = 0;
                }

                // read next portion from stream
                if ((read = stream.Read(buffer, total, readSize)) == 0)
                    break;

                total += read;
            }
            return (Bitmap)Bitmap.FromStream(new MemoryStream(buffer, 0, total));
        }

        public static void StartContinuousFetch()
        {
            if (!fetching)
            {
                fetching = true;
                Thread thread = new Thread(FetchEngine);
                thread.Start();
            }
        }

        public static void StopContinuousFetch()
        {
            fetching = false;
        }

        private static void FetchEngine()
        {
            while (fetching)
            {
                FetchedImage?.Invoke(FetchOne(), EventArgs.Empty);
                stopEvent.WaitOne(250, false);
            }
        }
    }
}
